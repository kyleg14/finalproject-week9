/// Geolocation Script that tracks location of browser ///

window.addEventListener('load', () => {
    let long;
    let lat;
    let temperatureDescription = document.querySelector('.temperature-description');
    let temperatureDegree = document.querySelector('.temperature-degree');
    let locationTimezone = document.querySelector('.location-timezone');

    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;

            const api = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&units=imperial&appid=${API_KEY}`
            
            fetch(api)
            .then(response => {
                return response.json();
            })
            .then(data => {
                console.log(data);  ///Remove later to clean up
                const temp = (Math.round(data['main']['temp']));
                const main = data['weather'][0]['description'];
                temperatureDegree.textContent = temp;
                temperatureDescription.textContent = main;
                locationTimezone.textContent = data.name;
                const locationIcon = document.querySelector('.weather-icon');
                const {icon} = data.weather[0];
                locationIcon.innerHTML = `<img src="icons/${icon}.png">`;
            });
        });
    }else{
        h1.textContent = "Please enable your browser location services."
}});

/// Clock between Weather/Notes ///

const clock12 = document.getElementById('clock12')

function concatZero(timeFrame) {
  return timeFrame < 10 ? '0'.concat(timeFrame) : timeFrame
}

function realTime() {
  let date = new Date()
  let sec = date.getSeconds()
  let mon = date.getMinutes()
  let hr = date.getHours()
  clock12.textContent = `${concatZero((hr % 12) || 12)}:${concatZero(mon)}:${concatZero(sec)} ${hr >= 12 ? 'PM' : 'AM'}`
}

setInterval(realTime, 1000)

/// Work Notes ///

displayNotes();
var addBtn = document.getElementById('addBtn');

/// Adding User Input in Local Storage ///
addBtn.addEventListener('click',function(){
	
	let notesObj;
	let addNote = document.getElementById('addNote');
	let notesString = localStorage.getItem('notes');
	
	if(notesString == null){
		notesObj = [];
	}
	else{
		notesObj = JSON.parse(notesString);
	}
	
	///  Add date ///
	let now = new Date();
	let dateTime = `${now.getMonth()}-${now.getDate()+1}-${now.getFullYear()} | ${now.getHours() % 12 || 12}:${now.getMinutes()}`;
	
	
	/// Push into Local Storage
	if(addNote.value !== ''){
		let tempObj = { text: addNote.value, time: dateTime };
		
		notesObj.push(tempObj);
		localStorage.setItem('notes',JSON.stringify(notesObj));
		
		addNote.value = '';
	}
	
	displayNotes();
});


/// Displays Local Storage data ///
function displayNotes(){
	let notesObj;
	let notesString = localStorage.getItem('notes');
	
	if(notesString == null){
		notesObj = [];
	}
	else{
		notesObj = JSON.parse(notesString);
	}
	
	let html = '';
	
	notesObj.forEach(function(element,index){
		html += `
				<div class="card mx-4 my-2 bg-dark text-white thatsMyNote" style="width: 18rem;">
					<div class="card-body">
						<h6>${element.time}</h6>
						<p class="card-text">${element.text.replace(/</g, "&lt;").replace(/>/g, "&gt;")}</p>
						<button id="${index}" onclick=deleteNote(this.id) class="btn btn-danger">Delete</button>
					</div>
				</div>
			`;
	});
	
	let noteEle = document.getElementById('notes');
	
	if(notesObj.length != 0){
		noteEle.innerHTML = html;
	}
	else{
		noteEle.innerHTML = '<h3 style="text-align: center; color: grey;">No Notes to Display</h3>';
	}
	
}


/// Note deleting function ///
function deleteNote(index){
	let notesObj;
	let notesString = localStorage.getItem('notes');
	
	if(notesString == null){
		notesObj = [];
	}
	else{
		notesObj = JSON.parse(notesString);
	}
	
	notesObj.splice(index,1);
	localStorage.setItem('notes',JSON.stringify(notesObj));
	
	displayNotes();
}



let search = document.getElementById('search');
search.addEventListener('input',function(e){
	
	let inputText = search.value;
	
	if(inputText == ''){
		document.getElementById('noMatches').innerHTML = '';
	}
	
	var countNone = 0;
	
	let cards = document.getElementsByClassName('thatsMyNote');
	
	
	Array.from(cards).forEach(function(ele){
		let cardText = ele.getElementsByTagName('p')[0].innerText;
		if(cardText.includes(inputText)){
			ele.style.display = 'block';
		}
		else{
			ele.style.display = 'none';
			
			countNone++;
			
			if(countNone === cards.length){
				document.getElementById('noMatches').innerHTML = '<h3 style="text-align: center; color: grey;">No matches found</h3>';
			}
			else{
				document.getElementById('noMatches').innerHTML = '';
			}
		}
	});
	
	/// Should display no matches found if nothing in return ///
	if(countNone === 0){
		document.getElementById('noMatches').innerHTML = '';
	}
});